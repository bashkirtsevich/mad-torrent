﻿MAD Torrent
===========

Самописный битторрент-клиент.

Среда разработки
----------------

Embarcadero RAD Studio XE* (Delphi)

Рекомендации к оформлению кода
------------------------------

http://wiki.delphi-jedi.org/index.php?title=Style_Guide

Добавление файлов
-----------------

В проект добавлять только файлы со следующими расширениями:

pas - исходный текст

dfm - исходный текст формы

fmx - исходный текст Firemonkey формы (без форм)

dfm - исходный текст Delphi формы

dpr - файл проекта

dproj - файл проекта

groupproj - группа проектов

html - веб страницы